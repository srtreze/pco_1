package pco.melody;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Locale;
import java.util.Scanner;

/**
 * 
 * Class that can read or save music files in the form of text.<br>
 * This simple text files have the following content format:<br>
 * <p>
 * line 1 - contains title;<br>
 * line 2 - contains author;<br>
 * line 3 - contains the number of notes in the melody, written in numbers;<br>
 * line 4 and beyond -  contain a number of notes, one each line, equal to the number of notes given previously. There cannot be a blank line in between.<br>
 * Each note line must have the following format: "duration pitch octave acc". If the note is silent, it will only contain "duration pitch"
 *
 *@author group039, Jorge Ferreira n43104, Joao Rodrigues n45802
 *
 */
public class MelodyIO {

  
	/***
	 * 
	 * Loads a .txt file and creates an object type Melody from said file.
	 * 
	 * @param file Represents a music file in the form of text. File is a .txt
	 * @return the Melody object (melodia) created from the text file
	 * @throws IOException if file does not exist, or there's a problem with it
	 */
	
	 //@requires file exists, is not corrupted and the file content has the right content format
	
	public static Melody load(File file) throws IOException {
		
		try {
		
			Scanner sc = new Scanner(file);
			sc.useLocale(Locale.ENGLISH);
			
			String titulo = sc.nextLine();
			String autor = sc.nextLine();
			int num_notas = Integer.parseInt(sc.nextLine());
	
			Melody melodia = new Melody(titulo, autor, num_notas);
			
			for (int conta_notas=0; conta_notas < num_notas; conta_notas++) {
				double duracao = sc.nextDouble();
				Pitch tom = Pitch.valueOf(sc.next());
				if (tom == Pitch.S) {
					Note nota = new Note(duracao);
					melodia.set(conta_notas, nota);
				} else {
					int oitava = Integer.parseInt(sc.next());
					Acc acidente = Acc.valueOf(sc.next());
					Note nota = new Note(duracao, tom, oitava, acidente);
					melodia.set(conta_notas, nota);
				}
			}
			sc.close();
			return melodia;
		}catch (IOException e){
			throw new IOException("O ficheiro de musica naoo foi encontrado");
		}
	}

	/***
	 * 
	 * Saves an object type Melody in a .txt file
	 * 
	 * @param melody Represents the melody that will be saved onto the file
	 * @param file Represents the new file, where the melody will be saved
	 * @throws IOException if the file where the melody will be saved does not exist, or there's a problem with it
	 * @throws IOException if it wasn't possible to save the melody onto the file
	 */
	
	 //@requires file exists and is not corrupted
	public static void save(Melody melody, File file) throws IOException {
		
		try {
		
			PrintStream out = new PrintStream(file);
	
			out.append(melody.getTitle()+"\n");
			out.append(melody.getAuthor()+"\n");
			out.append(melody.notes()+"\n");
			for (int i=0; i < melody.notes(); i++) {
				out.append(melody.get(i).toString()+"\n");
			}
	    
			out.close();
			
		}catch (IOException e){
				throw new IOException("Nao foi possivel guardar a musica");
		}
	}
  
	/***
	 * 
	 * Constructor
	 * 
	 */
	
	private MelodyIO() { }

}
