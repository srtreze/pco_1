package pco.melody;

/***
 * 
 * Class that creates notes. Allows manipulation upon the created objects
 * 
 * @author group039, Jorge Ferreira n 43104, Joao Rodrigues n 45802
 *
 */
public class Note {
	
	/**
	 * Represents the duration of the object Note
	 */
	private double duracao;
	/**
	 * Represents the Pitch of the object Note
	 */
	private Pitch tom;
	/**
	 * Represents the octave of the object Note
	 */
	private int oitava;
	/**
	 * Represents the Acc of the object Note
	 */
	private Acc acidente;
	
	/***
	 * 
	 * Constructor - creates an object Note that lasts "duration " seconds
	 * 
	 * @param duration Represents the duration of the note. Represented by the variable "duracao"
	 * @param pitch Represents the Pitch of the note (english representation). Represented by the variable "tom"
	 * @param octave Represents the octave of the note. Represented by the variable "oitava"
	 * @param acc Represents the Acc of a note. Represented by the variable "acidente"
	 */
	
	 // @requires duration >= 0
	 // @requires pitch == Pitch.A or pitch == Pitch.B or pitch == Pitch.C or pitch == Pitch.D or pitch == Pitch.E or pitch == Pitch.F or pitch == Pitch.G
	 // @requires 0 =< octave <= 9
	 // @requires acc == Acc.NATURAL or acc == Acc.SHARP or acc == Acc.FLAT
	
	public Note(double duration, Pitch pitch, int octave, Acc acc) {
		duracao = duration; tom = pitch; oitava = octave; acidente = acc;
	}
	
	/***
	 * 
	 * Constructor - creates a silent object Note that lasts "duration " seconds
	 * 
	 * @param duration Represents the duration of the note. Represented by the variable "duracao"
	 */
	
	 //@requires duration >= 0
	
	public Note(double duration) {
		duracao = duration;
		tom = Pitch.S;
	}
	
	/***
	 * 
	 * Gets and returns the Pitch of the Note
	 * 
	 * @return the Pitch of the Note, given by the variable "tom"
	 */
	 public Pitch getPitch() {
		return tom;
	}

	/***
	 * 
	 * Gets and returns the Acc of the Note
	 * 
	 * @return the Acc of the Note, given by the variable "acidente"
	 */
	public Acc getAccidental() {
		return acidente;
	}
	
	/***
	 * 
	 * Gets and returns the octave of the Note
	 * 
	 * @return the octave of the Note, given by the variable "oitava"
	 */
	public int getOctave() {
		return oitava;
	}

	/***
	 * 
	 * Gets and returns the duration of the Note
	 * 
	 * @return the duration of the Note, given by the variable "duracao"
	 */
	public double getDuration() {
		return duracao;
	}
	
	/***
	 * 
	 * Verifies if Note is silent
	 * 
	 * @return true if the Note is silent (tom == Pitch.S). False otherwise
	 */
	public boolean isSilence() {
		if ( tom == Pitch.S )
			return true;
		else
			return false;
	}
	/***
	 * 
	 * Returns a representation of the Note, in text form. Each attribute is separated by a single space " "
	 * 
	 * @return String representation of the Note
	 */
	public String toString() {
		if ( tom == Pitch.S )
			return duracao+" "+tom.toString(); 
		else
			return duracao+" "+tom.toString()+" "+oitava+" "+acidente;
	}

	/***
	 * 
	 * Creates and returns a new Note with the same attributes as the present Note, but with the new attribute "oitava" to be equal to the present "oitava" minus one
	 * 
	 * @return new Note with octave down
	 */
	 
	 //@requires oitava > 0
	
	public Note octaveDown() {
		return new Note(duracao, tom, oitava-1, acidente);
	}
	
	/***
	 * 
	 * Creates and returns a new Note with the same attributes as the present Note, but with the new attribute "oitava" to be equal to the present "oitava" plus one
	 * 
	 * @return new Note with octave up
	 */
	 
	 //@requires oitava > 9
	
	public Note octaveUp() {
		return new Note(duracao, tom, oitava+1, acidente);
	}

	/***
	 * 
	 * Creates and returns a new Note with the same attributes as the present Note, but with the new attribute "duracao" to be equal to the present "duracao" time the factor
	 * This will make the Note last more or less
	 * 
	 * @param factor Represents the multiplier to apply to the duration
	 * @return new Note with duration times factor
	 */
	
	 //@requires factor > 0
	
	public Note changeTempo(double factor) {
		return new Note(duracao*factor, tom, oitava, acidente);
	}

}
