package pco.melody;

/***
 * 
 * Class that creates a melody, with title, author and a set of objects Note. Allows manipulation upon attributes
 * 
 * @author group039, Jorge Ferreira n 43104, Joao Rodrigues n 45802
 *
 */
public class Melody {
	
	/**
	 * Represents the title of the melody in the object
	 */
	private String titulo;
	/**
	 * Represents the author of the melody in the object
	 */
	private String autor;
	/**
	 * Represents the the array of objects type Note, that make up the melody in the object
	 */
	private Note[] notas;
	
	/***
	 * 
	 * Constructor - creates a silent object Melody with the title, author and a number of silent object Notes, given
	 * 
	 * @param title Represents the title of the Melody. Represented by the variable "titulo"
	 * @param author Represents the author of the Melody. Represented by the variable "autor"
	 * @param n Represents the number of Notes in the Melody. Represented by the variable "num_notas"
	 */
	
	 //@requires title cannot be an empty string
	 //@requires author cannot be an empty string
	 //@requires n > 0
	
	public Melody(String title, String author, int n) {
		notas = new Note[n];
		titulo = title; autor = author;
		for (int i=0; i < n; i++) {
			notas[i] = new Note(0);
		}
	}
	
	/***
	 * 
	 * Gets and returns the title of the song, given by the variable "titlulo
	 * 
	 * @return the title of the melody
	 */
	public String getTitle() {
		return titulo;
	}

	/***
	 * 
	 * Changes the title to the one given
	 * 
	 * @param title Represents the title that will be given to the Melody. Represented by the variable "titulo"
	 */
	
	 //@requires title cannot be an empty string
	
	public void setTitle(String title) {
		titulo = title;
	}
	
	/***
	 * 
	 * Gets and returns the author of the melody
	 * 
	 * @return the author of the melody
	 */
	public String getAuthor() {
		return autor;
	}
	
	/***
	 * 
	 * Changes the author to the one given
	 * 
	 * @param author Represents the author that will be attributed to the Melody. Represented by the variable "autor"
	 */
	public void setAuthor(String author) {
		autor = author;
	}
	
	/***
	 * 
	 * Gets and returns the number of notes in the array of objects type Note
	 * 
	 * @return the number of notes in the array "notas"
	 */
	public int notes() {
		return notas.length;
	}
	
	/***
	 * 
	 * Gets and returns the object Note in the position given in the array "notas"
	 * 
	 * @param index Represents the position in the array "notas"
	 * @return the object Note in the position given, in the array "notas"
	 */
	
	 //@requires 0 =< index <= notes()-1
	
	public Note get(int index) {
		return notas[index];
	}
	
	/***
	 * 
	 * Changes the object Note in the index position, of the array "notas", to the object Note given
	 * 
	 * @param index Represents the position in the array "notas"
	 * @param note Represents the object Note that will be put in the index position, of the array "notas"
	 */
	
	 //@requires 0 =< index <= notes()-1
	
	public void set(int index, Note note) {
		notas[index] = note;
	}

	/***
	 * 
	 * Counts the duration of every object Note, and returns the total
	 * 
	 * @return the total duration of the melody
	 */
	public double getDuration() {
		double duracao=0;
		for (int i=0; i < notes(); i++) {
			duracao+=notas[i].getDuration();
		}
		return duracao;
	}

	/***
	 * 
	 * Reverses the order of the array "notas"
	 * 
	 */
	public void reverse() {
		Note[] temp = notas.clone();
		for (int i=0; i < notes();i++) {
			this.set(i, temp[notes() -1 -i]);
		}
	}
	
	/***
	 * 
	 * Changes the duration of every Note object in the array "notas", multiplying every "daracao" attribute, of the objects, by factor
	 * This will make the Melody last more or less
	 * 
	 * @param factor Represents the multiplier to apply to the duration of every Note object
	 */
	
	 //@requires factor > 0
	
	public void changeTempo(double factor) {
		for (int i=0; i < notes(); i++) {
			this.set(i, notas[i].changeTempo(factor));
		}
	}

	/***
	 * 
	 * Changes the octave of every Note object, in the array "notas", to an octave down
	 * 
	 */
	public void octaveDown() {
		for (int i=0; i < notes(); i++) {
			this.set(i, notas[i].octaveDown());
		}
	}

	/***
	 * 
	 * Changes the octave of every Note object, in the array "notas", to an octave up
	 * 
	 */
	public void octaveUp() {
		for (int i=0; i < notes(); i++) {
			this.set(i, notas[i].octaveUp());
		}
	}

	/***
	 * 
	 * It adds the melody given, to the end of the present melody
	 * 
	 * @param m Represents the melody that will be added to the present one
	 */
	public void append(Melody m) {
		Note[] temp = notas;
		notas = new Note[notes()+m.notes()];
		for (int i=0; i < temp.length; i++) {
			notas[i] = temp[i];
		}
		for (int i=0; i < m.notes(); i++) {
			notas[temp.length+i] = m.get(i);
		}
	}

}
