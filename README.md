# 1st PCO Project
### Grade - 19

## A simple music editor in Java

The project takes in a text (.txt) file with the notes and reproduces it. You're then able to do some simple edits:
- change durantion by _f_ factor (can be positive or negative);
- go an octive up;
- go an octive down;
- change title;
- change author;
- set the note with the _i_ index to the _n_ note;
- revert the melody;
- append another melody to the end of the curent one.

A skeleton Eclipse project was provided to us, with a few text files containing melody example. And the project was executed by running _src/pco/melody/Program.java_.

### To be developet was:

#### The _Note_ class:
- Note(double duration, Pitch pitch, int octave, Acc acc);
- Note(double duration);
- double getDuration();
- Pitch getPitch();
- int getOctave();
- Acc getAccidental();
- boolean isSilence();
- String toString();
- Note changeTempo(double f);
- Note octaveUp();
- Note octaveDown().

#### The _Melody_ class:
- Melody(String title, String author, int n);
- String getTitle();
- void setTitle(String title);
- String getAuthor();
- void setAuthor(String author);
- int notes();
- double getDuration();
- Note get(int i);
- void set(int i, Note n);
- void changeTempo(double f);
- void octaveUp();
- void octaveDown();
- void reverse();
- void append(Melody other).

#### The _MelodyIO_ class:
- static Melody load(File f);
- static void save(File f, Melody m).

### Exemple os a text file to be read:
>>>
Happy Birthday

Patty Hill and Mildred J. Hill

27

0.25 S

0.25 D 4 NATURAL

0.25 D 4 NATURAL

0.5 E 4 NATURAL

0.5 D 4 NATURAL

0.5 G 4 NATURAL

1 F 4 SHARP

0.25 D 4 NATURAL

0.25 D 4 NATURAL

0.5 E 4 NATURAL

0.5 D 4 NATURAL

0.5 A 4 NATURAL

1 G 4 NATURAL

0.25 D 4 NATURAL

0.25 D 4 NATURAL

0.5 D 5 NATURAL

0.5 B 4 NATURAL

0.5 G 4 NATURAL

0.5 F 4 SHARP

1 E 4 NATURAL

0.25 C 5 NATURAL

0.25 C 5 NATURAL

0.5 B 4 NATURAL

0.5 G 4 NATURAL

0.5 A 4 NATURAL

1.5 G 4 NATURAL

0.25 S
>>>